package com.example.demo.service;

import com.example.demo.entity.Venta;

public interface VentaService extends ICRUDService<Venta, Integer>{

	Venta registarVenta(Venta venta);

	Venta listarPoridPersona(Integer idPersona);

}
