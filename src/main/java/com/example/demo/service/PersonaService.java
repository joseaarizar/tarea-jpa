package com.example.demo.service;

import com.example.demo.entity.Persona;

public interface PersonaService  extends ICRUDService<Persona, Integer>{

}
