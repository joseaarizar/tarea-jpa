package com.example.demo.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.DetalleVenta;
import com.example.demo.entity.Venta;
import com.example.demo.repository.IGenericRepo;
import com.example.demo.repository.VentaRepository;
import com.example.demo.service.VentaService;

@Transactional
@Service
public class VentaServiceImpl extends CRUDImpl<Venta, Integer> implements VentaService {

	@Autowired
	private VentaRepository venRepository;

	@Override
	protected IGenericRepo<Venta, Integer> getRepo() {
		return venRepository;
	}

	@Override
	public Venta registarVenta(Venta venta) {
		
		//venta.getDetalleVenta().forEach(det -> det.setVenta(venta));
		
		//llena el objeto venta que se encuentra en el detalle_venta evitando que sea un objeto nulo para la bd
		for (DetalleVenta out : venta.getDetalleVenta()) {
			out.setVenta(venta);
		}
		
		venRepository.save(venta);
		return venta;
	}

	@Override
	public Venta listarPoridPersona(Integer idPersona) {
		Venta result = venRepository.buscarPorIdPersona(idPersona);
		return result;
	}

}
