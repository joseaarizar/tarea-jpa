package com.example.demo.service.impl;

import java.util.List;

import com.example.demo.repository.IGenericRepo;
import com.example.demo.service.ICRUDService;

public abstract class CRUDImpl<T, ID> implements ICRUDService<T, ID> {

	protected abstract IGenericRepo<T, ID> getRepo();

	@Override
	public T registrar(T t) {
		return getRepo().save(t);
	}

	@Override
	public T modificar(T t) {
		return getRepo().save(t);
	}

	@Override
	public List<T> listar() {
		return getRepo().findAll();
	}

	@Override
	public T listarPorId(ID id) {
		return getRepo().findById(id).orElse(null);
	}

	@Override
	public void Eliminar(ID id) {
		getRepo().deleteById(id);
	}

}
