package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Persona;
import com.example.demo.repository.IGenericRepo;
import com.example.demo.repository.PersonaRepository;
import com.example.demo.service.PersonaService;

@Service
public class PersonaServiceImpl extends CRUDImpl<Persona, Integer> implements PersonaService{

	@Autowired
	private PersonaRepository perRepository;
	
	@Override
	protected IGenericRepo<Persona, Integer> getRepo() {
		return perRepository;
	}

}
