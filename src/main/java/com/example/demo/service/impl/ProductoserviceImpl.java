package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Producto;
import com.example.demo.repository.IGenericRepo;
import com.example.demo.repository.ProductoRepository;
import com.example.demo.service.ProductoService;

@Service
public class ProductoserviceImpl extends CRUDImpl<Producto, Integer> implements ProductoService {

	@Autowired
	private ProductoRepository proRepository;
	
	@Override
	protected IGenericRepo<Producto, Integer> getRepo() {
		return proRepository;
	}

}
