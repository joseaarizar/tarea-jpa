package com.example.demo.exception;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ExceptionResponse {

	private LocalDateTime fecha;
	private List<ErrorInfo> listaError;

	public void addError(String mensaje, String detalle) {
		if (this.listaError == null) {
			this.listaError = new ArrayList<>();
		}
		
		ErrorInfo info = new ErrorInfo();
		info.setMensaje(mensaje);
		info.setDetalles(detalle);
		this.listaError.add(info);
	}
	

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}


	public List<ErrorInfo> getListaError() {
		return listaError;
	}


	public void setListaError(List<ErrorInfo> listaError) {
		this.listaError = listaError;
	}



	
	

}
