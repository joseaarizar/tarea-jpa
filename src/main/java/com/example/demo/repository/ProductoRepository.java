package com.example.demo.repository;

import com.example.demo.entity.Producto;

public interface ProductoRepository extends IGenericRepo<Producto, Integer>{

}
