package com.example.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Venta;

public interface VentaRepository extends IGenericRepo<Venta, Integer> {
	
	@Query(value = "SELECT *  FROM VENTA V INNER JOIN PERSONA P ON V.ID_PERSONA = P.ID_PERSONA WHERE V.ID_PERSONA = :idPersona", nativeQuery = true)
	Venta buscarPorIdPersona(@Param("idPersona") Integer idPersona);

}
