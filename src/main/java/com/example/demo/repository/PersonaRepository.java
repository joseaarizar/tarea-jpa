package com.example.demo.repository;

import com.example.demo.entity.Persona;

public interface PersonaRepository extends IGenericRepo<Persona, Integer> {

}
