package com.example.demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Persona;
import com.example.demo.service.PersonaService;

@RestController
@RequestMapping("/tarea")
public class PersonaController {

	@Autowired
	private PersonaService service;

	@GetMapping(value = "/persona")
	public ResponseEntity<List<Persona>> listar() throws Exception {
		List<Persona> listar = service.listar();
		return new ResponseEntity<List<Persona>>(listar, HttpStatus.OK);
	}

	@PostMapping(value = "/persona")
	public ResponseEntity<Persona> guardar(@Valid @RequestBody Persona p) throws Exception {
		Persona result = service.registrar(p);
		return new ResponseEntity<Persona>(result, HttpStatus.OK);

	}

	@GetMapping(value = "/persona/{id}")
	public ResponseEntity<Persona> BuscarPorId(@PathVariable("id") Integer id) throws Exception {
		Persona result = service.listarPorId(id);
		return new ResponseEntity<Persona>(result, HttpStatus.OK);

	}

	@PutMapping(value = "/persona")
	public ResponseEntity<Persona> modificar(@Valid @RequestBody Persona p) throws Exception {
		Persona result = service.modificar(p);
		return new ResponseEntity<Persona>(result, HttpStatus.OK);
	}

	@DeleteMapping("/persona/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		service.Eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
