package com.example.demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Producto;
import com.example.demo.service.ProductoService;

@RestController
@RequestMapping("/tarea")
public class ProductoController {

	@Autowired
	private ProductoService service;

	@GetMapping(value = "/producto")
	public ResponseEntity<List<Producto>> listar() throws Exception {
		List<Producto> listar = service.listar();
		return new ResponseEntity<List<Producto>>(listar, HttpStatus.OK);
	}

	@PostMapping(value = "/producto")
	public ResponseEntity<Producto> guardar(@Valid @RequestBody Producto p) throws Exception {
		Producto result = service.registrar(p);
		return new ResponseEntity<Producto>(result, HttpStatus.OK);
	}

	@GetMapping(value = "/producto/{id}")
	public ResponseEntity<Producto> BuscarPorId(@PathVariable("id") Integer id) throws Exception {
		Producto result = service.listarPorId(id);
		return new ResponseEntity<Producto>(result, HttpStatus.OK);
	}

	@PutMapping(value = "/producto")
	public ResponseEntity<Producto> modificar(@Valid @RequestBody Producto p) throws Exception {
		Producto result = service.modificar(p);
		return new ResponseEntity<Producto>(result, HttpStatus.OK);
	}

	@DeleteMapping("/producto/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		service.Eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
