package com.example.demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Venta;
import com.example.demo.service.VentaService;

@RestController
@RequestMapping("/tarea")
public class VentaController {

	@Autowired
	private VentaService service;

	@PostMapping(value = "/venta")
	public ResponseEntity<Venta> registrarVenta(@Valid @RequestBody Venta venta)throws Exception {
		Venta result = service.registarVenta(venta);
		return new ResponseEntity<Venta>(result, HttpStatus.OK);

	}

	@GetMapping(value = "/venta")
	public ResponseEntity<List<Venta>> buscarVentas() {
		List<Venta> result = service.listar();
		return new ResponseEntity<List<Venta>>(result, HttpStatus.OK);

	}

	@GetMapping(value = "/venta/{id}")
	public ResponseEntity<Venta> BuscarPorId(@PathVariable("id") Integer id) {
		Venta result = service.listarPorId(id);
		return new ResponseEntity<Venta>(result, HttpStatus.OK);

	}

	@GetMapping(value = "/venta/persona/{idPersona}")
	public ResponseEntity<Venta> BuscarPorIdPersona(@PathVariable("idPersona") Integer idPersona) {
		Venta result = service.listarPoridPersona(idPersona);
		return new ResponseEntity<Venta>(result, HttpStatus.OK);

	}

}
